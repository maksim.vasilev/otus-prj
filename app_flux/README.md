# Install Stan's Robot Shop to your Kubernetes cluster using the helm chart.
cd app/helm
kubectl create ns robot-shop
helm install robot-shop --namespace robot-shop .
helm upgrade --install robot-shop --namespace robot-shop .


# Получим ip адрес нашего приложения для дальнейшей его подстановки в load-deployment.yaml
kubectl get svc/web -n robot-shop | tail -n 1 | sed 's/   /:/g' | cut -d : -f4