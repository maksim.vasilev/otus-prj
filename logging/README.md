Создание необходимых namespace:
cd monitoring
kubectl apply -f create_namespace.yaml

Добавляем helm репозитории:
helm repo add elastic https://helm.elastic.co

Установка ingress:
helm upgrade --install nginx-ingress stable/nginx-ingress --namespace nginx

Установка EFK
helm upgrade --install elasticsearch elastic/elasticsearch --namespace logging -f logging/elasticsearch.values.yaml
helm upgrade --install kibana elastic/kibana --namespace logging -f logging/kibana.values.yaml
helm upgrade --install fluent-bit stable/fluent-bit --namespace logging -f logging/fluentbit.values.yaml


