provider "google" {
credentials = file("account.json")
project = "glowing-market-273916"
zone = "us-central1"

}

locals {
zone = "us-central1"
}

resource "google_container_cluster" "default" {
name = "otus"
location = "us-central1-a"
min_master_version = "1.15.11-gke.9"
initial_node_count = "1"
  

logging_service = "none"
monitoring_service = "none"

remove_default_node_pool = true

network_policy {
    enabled = true
    provider = "CALICO"
  }

master_auth {
username = ""
password = ""

client_certificate_config {
issue_client_certificate = false
}
}
}


resource "google_container_node_pool" "infra" {
location = "${google_container_cluster.default.location}"
cluster = "${google_container_cluster.default.name}"
version = "1.15.11-gke.9"
initial_node_count = "3"
name               = "infra"

# logging_service = "false"
# monitoring_service = "false"

autoscaling {
    min_node_count = 1
    max_node_count = 5
  }


node_config {
machine_type = "e2-standard-2"

labels = {
node_pool = "infra"
release = "istio"
}




taint {
      effect = "PREFER_NO_SCHEDULE"
      key    = "infra"
      value  = "gke-${google_container_cluster.default.name}-infra"
    }

tags = [
"gke-${google_container_cluster.default.name}",
"gke-${google_container_cluster.default.name}-infra",
"gke-infra"
]


 oauth_scopes = ["storage-ro", "logging-write", "monitoring"]
}

}



resource "google_container_node_pool" "app" {
location = "${google_container_cluster.default.location}"
cluster = "${google_container_cluster.default.name}"
version = "1.15.11-gke.9"
initial_node_count = "1"
name               = "app"

# logging_service = "false"
# monitoring_service = "false"

autoscaling {
    min_node_count = 1
    max_node_count = 5
  }


node_config {
machine_type = "e2-standard-2"

labels = {
node_pool = "app"
}


tags = [
"gke-${google_container_cluster.default.name}",
"gke-${google_container_cluster.default.name}-app",
]

}

}
