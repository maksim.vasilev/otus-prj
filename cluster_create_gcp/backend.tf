terraform {
  backend "gcs" {
    bucket = "otusprj"
    prefix = "terraform/state"
  }
}
