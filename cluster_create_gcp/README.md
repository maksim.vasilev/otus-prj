# Для ручного запуска кластера из манифеста достаточно запустить из папки cluster_create_gcp/ сл. команды:

# Для инициализации плагина провайдера
terraform init
# Для проверки манифестов
terragform plan
# Для создания описанных ресурсов
terraform apply
# Обновить kubectl для подключения к кластеру
gcloud container clusters get-credentials otus --region us-central1-a --project glowing-market-273916
# Для удаления ресурсов 
terraform destroy
