# Run project form conatiner

git clone https://gitlab.com/maksim.vasilev/otus-prj.git
cd ./otus-prj/cluster_create_gcp/
 CREATE account.json
terraform init 
terraform apply -auto-approve 
gcloud auth activate-service-account --key-file /root/otus-prj/cluster_create_gcp/account.json
gcloud container clusters get-credentials otus --region us-central1-a --project glowing-market-273916
gcloud compute firewall-rules create http-8080 --description "Incoming http on 8080 allowed." --allow tcp:8080 --project glowing-market-273916
cd /root/otus-prj/app/helm/
kubectl create namespace robot-shop
helm upgrade --install robot-shop --namespace robot-shop .